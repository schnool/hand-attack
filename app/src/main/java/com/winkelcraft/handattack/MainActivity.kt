package com.winkelcraft.handattack

import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        text.setText("")
    }

    fun go(v:View) {
        object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                text.setText("seconds remaining: " + millisUntilFinished / 1000)
            }

            override fun onFinish() {
                text.setText("done!")
            }
        }.start()
    }


}